@extends('layouts.rewards')
@section('content')

    <nav class="navbar navbar bg">
        Modulo Premiación
    </nav>
    <nav class="navbar navbar bg">
        <a href="{{route('rewards/add')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Agregar Premio</a>
    </nav>
    <br>
    <table id="General" class="table">
        <thead>

        <tr>
            <th>Id</th>
            <th>Sorteo</th>
            <th>Usuario</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($rewards as $reward)
            <tr>
                <td>{{ $reward->id }}</td>
                <td>{{ $reward->time_sub_gamblings_id }}</td>
                <td>{{ $reward->users_id }}</td>
                <td>
                    <a href="rewards/edit/{{ $reward->id }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Modificar</a>
                    <a href="rewards/delete/{{ $reward->id }}" class="btn btn-danger btn-mg active" role="button" aria-pressed="true">Eliminar</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
