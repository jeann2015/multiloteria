@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    <h2>
    Taquilla
    </h2>
</nav>
<nav class="navbar navbar bg">
    <a href="{{route('home')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
    <a target="_blank" href="{{ URL::to('sales') }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Ver Venta</a>
</nav>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (isset($messages) && $messages<>"")
    <div class="alert alert-danger">
        <ul>
                <li>{{ $messages }}</li>
        </ul>
    </div>
@endif
{!! Form::open(array('url' => 'booking/new')) !!}
    <table  class="table">
        <tr>
            <td>Juego:</td>
            <td>
                {!! Form::select('gamblings_id',$gamblings,[],array('class' => 'form-control','id'=>'gamblings_id','required','onclick'=>'search_sub_gamblings_time(this.value)')) !!}
            </td>
            <td>Sorteos:</td>
            <td>
                {!! Form::select('sub_gamblings_id',[],[],array('class' => 'form-control','id'=>'sub_gamblings_id','required','onclick'=>'search_time_sub_gamblings_time(this.value)')) !!}
            </td>
            <td>Hora:</td>
            <td>
                {!! Form::select('time_sub_gamblings_id[]',[],[],array('class' => 'form-control','id'=>'time_sub_gamblings_id','required','multiple'=>'true')) !!}
            </td>
        </tr>
        <tr>
            <td>Monto a Jugar:</td>
            <td colspan="2">
                {!! Form::number('amount','',array('class' => 'form-control','id'=>'amount')) !!}
            </td>

        </tr>
        <tr>
            <td colspan="6">
                <div align="center">
                {!! Form::submit('Jugar!',array('class' => 'btn btn-primary','id'=>'play')) !!}
                {!! Form::hidden('id_taquilla',$taquillas_id,array('class' => 'form-control','id'=>'id_taquilla','required')) !!}
                @if(isset($TicketsTemporal))
                    {!! Form::hidden('id_tickets_temporal',$TicketsTemporal->pluck('id_tickets_number')[0],array('class' => 'form-control','id'=>'id_tickets_temporal','required')) !!}
                @else
                    {!! Form::hidden('id_tickets_temporal','',array('class' => 'form-control','id'=>'id_tickets_temporal','required')) !!}
                @endif
                </div>
            </td>
        </tr>
        <tr>
            <td>Jugada:</td>
            <td colspan="5">
                @if(isset($TicketsTemporal))
                    <iframe width="1000" height="220" src="{{ URL::to('tickets/temporals/'.$TicketsTemporal->pluck('id_tickets_number')[0]) }}" frameborder="0"></iframe>
                @endif
            </td>
        </tr>
        <tr>
            <td>Total Jugada:</td>
            <td colspan="5">
                @if(isset($TicketsTemporal))
                {!! Form::text('amount_playing',number_format($TicketsTemporal->sum('amount'),2),array('class' => 'form-control','id'=>'amount_playing' ,'readonly')) !!}
                @else
                    {!! Form::text('amount_playing','0.00',array('class' => 'form-control','id'=>'amount_playing' ,'readonly')) !!}
                @endif
            </td>
        </tr>
    </table>
    {!! Form::close() !!}



<table class="table">
<tr>
<td>
{!! Form::open(array('url' => 'tickets/new')) !!}
{!! Form::submit('Facturar!',array('class' => 'btn btn-primary','id'=>'fact')) !!}
@if(isset($TicketsTemporal))
    {!! Form::hidden('id',$TicketsTemporal->pluck('id_tickets_number')[0],array('class' => 'form-control','id'=>'id','required')) !!}
@endif
{!! Form::close() !!}
</td>
</tr>
<tr>
<td colspan="5">

    {!! Form::open(array('url' => 'booking/destroy')) !!}

    {!! Form::submit('Eliminar Jugada General!',array('class' => 'btn btn-danger','id'=>'delete')) !!}
    @if(isset($TicketsTemporal))
    {!! Form::hidden('id',$TicketsTemporal->pluck('id_tickets_number')[0],array('class' => 'form-control','id'=>'id','required')) !!}
    @endif
    </nav>
    {!! Form::close() !!}
</td>
</tr>
</table>




</div>