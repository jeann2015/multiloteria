@extends('layouts.header')
@section('content')
@if (isset($messages) && $messages<>"")
    <div class="alert alert-danger">
        <ul>
            <li>{{ $messages }}</li>
        </ul>
    </div>
@endif
<nav class="navbar navbar bg">
    Ventas
</nav>
<div class="alert alert-success">
    <ul>
        <li>
            Total Vendido: {{ number_format($tickets->sum('amount'),2) }}
        </li>
    </ul>
</div>
<table id="General" class="table">
<thead>
<tr>
    <th>Numero de Tickets</th>
    <th>Jugada</th>
    <th>Vendido</th>
    <th>Fecha/Hora de Jugada</th>
    <th>Acccion</th>
</tr>
</thead>
<tbody>
@foreach ($tickets as $ticket)
    <tr>
        <td>{{ $ticket->id_tickets_number }}</td>
        <td>{{ $ticket->sub_gamblings_description }}</td>
        <td>{{ number_format($ticket->where('id_tickets_number',$ticket->id_tickets_number)->sum('amount'),2) }}</td>
        <td>{{ Carbon\Carbon::parse($ticket->created_at)->format('d-m-Y h:i:s A') }}</td>
        <td>
            <a href="tickets/delete/{{ $ticket->id_tickets_number }}" class="btn btn-danger btn-mg active" role="button" aria-pressed="true">Anular</a>
        </td>
    </tr>
@endforeach
</tbody>
</table>
