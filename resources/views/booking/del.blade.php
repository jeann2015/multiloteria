@extends('layouts.header')
@section('content')
<nav class="navbar navbar bg">
    Anular Ticket
</nav>
@if (isset($messages) && $messages<>"")
    <div class="alert alert-danger">
        <ul>
            <li>{{ $messages }}</li>
        </ul>
    </div>
@endif
<nav class="nav navbar bg">
    <a href="{{URL::to('sales')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<table id="" class="table">
<thead>
<tr>
    <th>Jugada</th>
    <th>Vendido</th>
    <th>Fecha/Hora de Jugada</th>
</tr>
</thead>
<tbody>
@foreach ($tickets as $ticket)
    <tr>
        <td>{{ $ticket->sub_gamblings_description }}</td>
        <td>{{ number_format($ticket->where('id_tickets_number',$ticket->id_tickets_number)->sum('amount'),2) }}</td>
        <td>{{ Carbon\Carbon::parse($ticket->created_at)->format('d-m-Y h:i:s A') }}</td>
    </tr>
@endforeach
@if (!isset($messages) || $messages=="")
<tr>
    <td>
        {!! Form::open(array('url' => '/tickets/destroy')) !!}
        {!! Form::submit('Anular Ticket!',array('class' => 'btn btn-danger','id'=>'delete')) !!}
        {!! Form::hidden('id',$ticket->id_tickets_number,array('class' => 'form-control','id'=>'id','required')) !!}
        {!! Form::close() !!}
    </td>
</tr>
@endif
</tbody>
</table>
