@extends('layouts.temporals')
@section('content')
<table id="" class="table table-striped">
<thead>
<tr class="info">
    <th>Item</th>
    <th>Jugada a pagar</th>
    <th>Accion</th>
</tr>
</thead>
<tbody>
@php
$con=1;
@endphp

@foreach ($TicketsTemporal as $TicketsTempora)
    <tr>
        <td>{{ $con }}</td>
        <td>{{ $TicketsTempora->sub_gamblings_description }}</td>
        @if($TicketsTemporal->count()>1)
            <td><a href="{{ URL::to('tickets/temporals/delete/'.$TicketsTempora->id .'/'.$TicketsTempora->id_tickets_number ) }}" class="btn btn-danger btn-mg active" role="button" aria-pressed="true">Eliminar</a></td>
        @else
            <td></td>
        @endif
    </tr>
    @php
        $con=$con+1;
    @endphp
@endforeach
</tbody>
</table>
