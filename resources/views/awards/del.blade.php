@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    Eliminar Premio
</nav>
<nav class="navbar navbar bg">
    <a href="{{route('awards')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<br>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
{!! Form::open(array('url' => 'awards/destroy')) !!}
    <table  class="table">
        <tr>
            <td>Descripcion:</td>
            <td>
                {!! Form::text('description',$awards->description,array('class' => 'form-control','id'=>'description','required')) !!}
            </td>
        </tr>
        <tr>
            <td>Monto Jugado:</td>
            <td>
                {!! Form::number('amount_play',$awards->amount_play,array('class' => 'form-control','id'=>'amount_play','required')) !!}
            </td>
        </tr>
        <tr>
            <td>Monto a Pagar:</td>
            <td>
                {!! Form::number('amount_pay',$awards->amount_pay,array('class' => 'form-control','id'=>'amount_pay','required')) !!}
            </td>
        </tr>
    </table>
    <nav class="navbar navbar bg">
        {!! Form::submit('Eliminar!',array('class' => 'btn btn-primary','id'=>'delete')) !!}
        {!! Form::hidden('id',$awards->id,array('class' => 'form-control','id'=>'id','required')) !!}
    </nav>
{!! Form::close() !!}
</div>