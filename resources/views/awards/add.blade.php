@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    Nuevo Premio
</nav>
<nav class="navbar navbar bg">
    <a href="{{route('awards')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<br>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
{!! Form::open(array('url' => 'awards/new')) !!}
    <table  class="table">
        <tr>
            <td>Juego:</td>
            <td>
                {!! Form::select('gamblings_id',$gamblings,[],array('class' => 'form-control','id'=>'gamblings_id','required','onclick'=>'search_sub_gamblings_time(this.value)')) !!}
            </td>
            <td>Sorteos:</td>
            <td>
                {!! Form::select('sub_gamblings_id',[],[],array('class' => 'form-control','id'=>'sub_gamblings_id','required','onclick'=>'search_time_sub_gamblings_time(this.value)')) !!}
            </td>
            <td>Hora:</td>
            <td>
                {!! Form::select('time_sub_gamblings_id[]',[],[],array('class' => 'form-control','id'=>'time_sub_gamblings_id','required','multiple'=>'true')) !!}
            </td>
        </tr>
    </table>
    <nav class="navbar navbar bg">
        {!! Form::submit('Guardar!',array('class' => 'btn btn-primary','id'=>'save')) !!}
    </nav>
{!! Form::close() !!}
</div>