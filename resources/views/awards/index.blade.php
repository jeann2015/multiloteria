@extends('layouts.header')
@section('content')

    <nav class="navbar navbar bg">
        Modulo Premios por Jugadas
    </nav>
    <nav class="navbar navbar bg">
        <a href="{{route('awards/add')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Agregar Premio</a>
    </nav>
    <br>
    <table id="General" class="table">
        <thead>

        <tr>
            <th>Id</th>
            <th>Descripcion</th>
            <th>Monto Jugado</th>
            <th>Monto a Pagar</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($awards as $award)
            <tr>
                <td>{{ $award->id }}</td>
                <td>{{ $award->description }}</td>
                <td>{{ $award->amount_play }}</td>
                <td>{{ $award->amount_pay }}</td>
                <td>
                    <a href="awards/edit/{{ $award->id }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Modificar</a>
                    <a href="awards/delete/{{ $award->id }}" class="btn btn-danger btn-mg active" role="button" aria-pressed="true">Eliminar</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
