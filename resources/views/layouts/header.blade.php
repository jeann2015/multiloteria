<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css">
    <link href="{{ asset('css/offcanvas.css') }}" rel="stylesheet">
    <!-- Styles -->

</head>
<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>
                    <a href="{{ route('register') }}">Register</a>
                    @endauth
        </div>
    @endif

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="{{route('home')}}">{{ config('app.name', 'Laravel') }}</a>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Principal</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="{{route('limits')}}">Limites</a>
                        <a class="dropdown-item" href="{{route('gamblings')}}">Juegos</a>
                        <a class="dropdown-item" href="{{route('booking')}}">Taquilla</a>
                        <a class="dropdown-item" href="{{route('awards')}}">Premios por Jugadas</a>
                        <a class="dropdown-item" href="{{route('rewards')}}">Premiación</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('logout')}}">Salir de Aplicacion</a>
                </li>
                </li>
            </ul>

        </div>
    </nav>

    </div>
        @yield('content')

</nav>


</body>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/locale/es.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>

<script>
    $(document).ready(function() {
        $('#General').DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },

            "order": [[ 0, "desc" ]]
        });
    });

    var url = "http://appbackend.online:8085/";
//    var url = "http://multiloteria.local/";

    function search_sub_gamblings(gamblings_id) {

        $('#sub_gamblings_id').find('option').remove().end();
        $('#time_sub_gamblings_id').find('option').remove().end();


        if(gamblings_id != "") {
            $.ajax({
                type: "get",
                url: url + "api/search/sub_gamblings/"+gamblings_id,
                dataType: "json",
                data: {},
                success: function (request) {

                    for (var i in request) {
                        $('#sub_gamblings_id').append("<option value='" + i + "'>" + request[i] + "</option>")
                    }
                }
            });
        }

    }

    function calcular() {

        $('#sub_gamblings_id').find('option').remove().end();
        $('#time_sub_gamblings_id').find('option').remove().end();
        var id_taquilla = $('#id_taquilla').val();

        if(gamblings_id != "" ) {
            $.ajax({
                type: "get",
                url: url + "api/search/sub_gamblings/time/" + gamblings_id+"/"+id_taquilla,
                dataType: "json",
                data: {},
                success: function (request) {

                    for (var i in request) {
                        $('#sub_gamblings_id').append("<option value='" + i + "'>" + request[i] + "</option>")
                    }
                }
            });
        }


    }

    function search_sub_gamblings_time(gamblings_id) {

        $('#sub_gamblings_id').find('option').remove().end();
        $('#time_sub_gamblings_id').find('option').remove().end();
        var id_taquilla = $('#id_taquilla').val();

        if(gamblings_id != "" ) {
            $.ajax({
                type: "get",
                url: url + "api/search/sub_gamblings/time/" + gamblings_id+"/"+id_taquilla,
                dataType: "json",
                data: {},
                success: function (request) {

                    for (var i in request) {
                        $('#sub_gamblings_id').append("<option value='" + i + "'>" + request[i] + "</option>")
                    }
                }
            });
        }


    }


    function search_time_sub_gamblings_time(sub_gamblings_id) {

        $('#time_sub_gamblings_id').find('option').remove().end();
        var id_taquilla = $('#id_taquilla').val();
        var id_tickets_temporal = $('#id_tickets_temporal').val();

        if(sub_gamblings_id != "" && id_tickets_temporal=="") {
            $.ajax({
                type: "get",
                url: url + "api/search/time_sub_gamblings/time/" + sub_gamblings_id+"/"+id_taquilla,
                dataType: "json",
                data: {},
                success: function (request) {

                    for (var i in request) {
                        $('#time_sub_gamblings_id').append("<option value='" + i + "'>" + request[i] + "</option>")
                    }
                }
            });
        }

        if(sub_gamblings_id != "" && id_tickets_temporal !="") {
            $.ajax({
                type: "get",
                url: url + "api/search/time_sub_gamblings/time/temporal/" + sub_gamblings_id+"/"+id_taquilla+"/"+id_tickets_temporal,
                dataType: "json",
                data: {},
                success: function (request) {

                    for (var i in request) {
                        $('#time_sub_gamblings_id').append("<option value='" + i + "'>" + request[i] + "</option>")
                    }
                }
            });
        }
    }
</script>

</html>
