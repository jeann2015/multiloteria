@extends('layouts.header')
@section('content')

    <div class="container">
    <br>
    <nav class="navbar navbar bg">
        Modificar Juego
    </nav>
    <nav class="navbar navbar bg">
        <a href="{{route('gamblings')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
    </nav>
    <br>
    {!! Form::open(array('url' => 'gamblings/update')) !!}
        <table class="table">
            <tr>
                <td>Nombre:</td>
                <td>
                    {!! Form::text('description',$gamblings->description,array('class' => 'form-control','id'=>'description','required')) !!}
                </td>
            </tr>
        </table>
        <nav class="navbar navbar bg">
            {!! Form::hidden('id',$gamblings->id,array('class' => 'form-control','id'=>'id','required')) !!}
            {!! Form::submit('Modificar!',array('class' => 'btn btn-primary','id'=>'save')) !!}
        </nav>
    {!! Form::close() !!}


        <nav class="navbar navbar bg">
            Sorteos
        </nav>
        <nav class="navbar navbar bg">
            <a href="{{ URL::to('subgamblings/subadd/'.$gamblings->id) }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Agregar Sorteo</a>
        </nav>
    <table id="General" class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Descripcion</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($subgamblings as $subgambling)
            <tr>
                <td>{{ $subgambling->id }}</td>
                <td>{{ $subgambling->description }}</td>
                @if( $subgambling->status==1)
                    <td><span class="badge badge-success"> {{'Activo'}}</span></td>
                @else
                    <td><span class="badge badge-danger"> {{'No Activo'}}</span></td>
                @endif
                <td>
                    <a href="{{ URL::to('subgamblings/subedit/'.$gamblings->id."/".$subgambling->id) }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Modificar General</a>
                    <a href="{{ URL::to('subgamblings/subdelete/'.$gamblings->id."/".$subgambling->id) }}" class="btn btn-danger btn-mg active" role="button" aria-pressed="true">Eliminar</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    </div>