@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    Nuevo Juego
</nav>
<nav class="navbar navbar bg">
    <a href="{{route('gamblings')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<br>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
{!! Form::open(array('url' => 'gamblings/new')) !!}
    <table  class="table">
        <tr>
            <td>Nombre:</td>
            <td>
                {!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!}
            </td>
        </tr>
    </table>
    <nav class="navbar navbar bg">
        {!! Form::submit('Guardar!',array('class' => 'btn btn-primary','id'=>'save')) !!}
    </nav>
{!! Form::close() !!}
</div>