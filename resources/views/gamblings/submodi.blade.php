@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    Modificar Sorteo
</nav>
<nav class="navbar navbar bg">
    <a href="{{URL::to('gamblings/edit/'.$gamblings->id) }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<br>
{!! Form::open(array('url' => 'subgamblings/subupdatei')) !!}
    <table class="table">

        <tr>
            <td>Descripcion:</td>
            <td>
                {!! Form::text('description',$subgamblings->description,array('class' => 'form-control','id'=>'description','required')) !!}
            </td>
        </tr>
        <tr>
            <td>Hora:</td>
            <td>
                {!! Form::select('hora',$hora,$horas,array('class' => 'form-control','id'=>'hora','required')) !!}
            </td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td>
                {!! Form::select('status',[1=>'Activo',0=>'No Activo'],$subgamblings->status,array('class' => 'form-control','id'=>'hora','required')) !!}
            </td>
        </tr>
    </table>
    <nav class="navbar navbar bg">
        {!! Form::hidden('id',$gamblings->id,array('class' => 'form-control','id'=>'id','required')) !!}
        {!! Form::hidden('subid',$subgamblings->id,array('class' => 'form-control','id'=>'subid','required')) !!}
        {!! Form::submit('Modificar!',array('class' => 'btn btn-primary','id'=>'update')) !!}
    </nav>
{!! Form::close() !!}
</div>