@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    Modificar Sorteo
</nav>
<nav class="navbar navbar bg">
    <a href="{{URL::to('gamblings/edit/'.$gamblings->id) }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<br>
{!! Form::open(array('url' => 'subgamblings/subupdate','enctype'=>'multipart/form-data')) !!}
    <table class="table">

        <tr>
            <td>Descripcion:</td>
            <td>
                {!! Form::text('description',$subgamblings->description,array('class' => 'form-control','id'=>'description','required')) !!}
            </td>
        </tr>
        <tr>
            <td>Hora:</td>
            <td>
                {!! Form::select('hora[]',$hora,$horas,array('class' => 'form-control','id'=>'hora','required','multiple'=>'true')) !!}
            </td>
        </tr>
        <tr>
            <td>Foto:</td>
            <td>
                @if(isset($pathToFile) && $pathToFile<>"")
                    <img src="{{ url( $pathToFile[1] ) }}" height="65" width="65">
                @endif
                {!! Form::file('images1', ['id'=>'images1','class'=>'form-control']) !!}
            </td>
        </tr>
    </table>
    <nav class="navbar navbar bg">
        {!! Form::hidden('id',$gamblings->id,array('class' => 'form-control','id'=>'id','required')) !!}
        {!! Form::hidden('subid',$subgamblings->id,array('class' => 'form-control','id'=>'subid','required')) !!}
        {!! Form::submit('Modificar!',array('class' => 'btn btn-primary','id'=>'update')) !!}
    </nav>
{!! Form::close() !!}
</div>