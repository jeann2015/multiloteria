@extends('layouts.header')
@section('content')

    <nav class="navbar navbar bg">
        Modulo Juegos
    </nav>
    <nav class="navbar navbar bg">
        <a href="{{route('gamblings/add')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Agregar Juego</a>
    </nav>
    <br>
    <table id="General" class="table">
        <thead>

        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($gamblings as $gambling)
            <tr>
                <td>{{ $gambling->id }}</td>
                <td>{{ $gambling->description }}</td>
                <td>
                    <a href="gamblings/edit/{{ $gambling->id }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Modificar</a>
                    <a href="gamblings/delete/{{ $gambling->id }}" class="btn btn-danger btn-mg active" role="button" aria-pressed="true">Eliminar</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
