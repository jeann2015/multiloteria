@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    Nuevo Sorteo
</nav>
<nav class="navbar navbar bg">
    <a href="{{URL::to('gamblings/edit/'.$gamblings->id) }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::open(array('url' => 'subgamblings/subnew','enctype'=>'multipart/form-data')) !!}
    <table class="table">

        <tr>
            <td>Descripcion:</td>
            <td>
                {!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!}
            </td>
        </tr>
        <tr>
            <td>Hora:</td>
            <td>
                {!! Form::select('hora[]',$hora,[],array('class' => 'form-control','id'=>'hora','required','multiple'=>'true')) !!}
            </td>
        </tr>
        <tr>
            <td>Foto:</td>
            <td>
                {!! Form::file('images1', ['id'=>'images1','class'=>'form-control']) !!}
            </td>
        </tr>
    </table>
    <nav class="navbar navbar bg">
        {!! Form::hidden('id',$gamblings->id,array('class' => 'form-control','id'=>'id','required')) !!}
        {!! Form::submit('Guardar!',array('class' => 'btn btn-primary','id'=>'save')) !!}
    </nav>
{!! Form::close() !!}
</div>