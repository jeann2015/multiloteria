@extends('layouts.header')
@section('content')

    <nav class="navbar navbar bg">
        Modulo Limites
    </nav>
    <nav class="navbar navbar bg">
        <a href="{{route('limits/add')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Agregar Limite</a>
    </nav>
    <br>
    <table id="General" class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Taquilla</th>
            <th>Juego</th>
            <th>Sorteo</th>
            <th>Hora</th>
            <th>Monto</th>
            <th>Premio a pagar</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($limits as $limit)
            <tr>
                <td>{{ $limit->id }}</td>
                <td>{{ $limit->nombre }}</td>
                <td>{{ $limit->juego }}</td>
                <td>{{ $limit->description }}</td>
                <td>{{ Carbon\Carbon::parse($limit->hora)->format('h:i A') }}</td>
                <td>{{ $limit->amount }}</td>
                <td>{{ $limit->description_awards }}</td>
                <td>
                    <a href="limits/editi/{{ $limit->id }}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Modificar</a>
                    <a href="limits/delete/{{ $limit->id_taquilla }}/{{$limit->gamblings_id}}" class="btn btn-danger btn-mg active" role="button" aria-pressed="true">Eliminar</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
