@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    Nuevo Limite
</nav>
<nav class="navbar navbar bg">
    <a href="{{route('limits')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::open(array('url' => 'limits/new')) !!}
    <table  class="table">
        <tr>
            <td>Taquilla:</td>
            <td>
                {!! Form::select('id_taquilla', $bookings, [], ['id'=>'id_taquilla','required','class'=>'form-control']) !!}
            </td>
        </tr>
        <tr>
            <td>Juego:</td>
            <td>
                {!! Form::select('gamblings_id', $gamblings, [], ['id'=>'gamblings_id','required','class'=>'form-control','onclick'=>'search_sub_gamblings(this.value)']) !!}
            </td>
        </tr>
        <tr>
            <td>Sorteo:</td>
            <td>
                {!! Form::select('sub_gamblings_id[]',[] , [], ['id'=>'sub_gamblings_id','class'=>'form-control ','multiple'=>'true']) !!}
            </td>
        </tr>
        <tr>
            <td>Monto Limite</td>
            <td>
                {!! Form::number('amount','0.00',array('class' => 'form-control','id'=>'amount','required')) !!}
            </td>
        </tr>
        <tr>
            <td>Pago por Jugadas:</td>
            <td>
                {!! Form::select('awards_id',$awards , [], ['id'=>'awards_id','class'=>'form-control','required']) !!}
            </td>
        </tr>
    </table>
    <nav class="navbar navbar bg">
        {!! Form::submit('Guardar!',array('class' => 'btn btn-primary','id'=>'save')) !!}
    </nav>

{!! Form::close() !!}
</div>