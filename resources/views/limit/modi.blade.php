@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    Modificar Limite Individual
</nav>
<nav class="navbar navbar bg">
    <a href="{{route('limits')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::open(array('url' => 'limits/updatei')) !!}
    <table  class="table">
        <tr>
            <td>Taquilla:</td>
            <td>
                {!! Form::select('id_taquilla', $bookings, $limitsOnly->id_taquilla, ['id'=>'id_taquilla','required','class'=>'form-control']) !!}
            </td>
        </tr>
        <tr>
            <td>Juego:</td>
            <td>
                {!! Form::select('gamblings_id', $gamblings, $limitsOnly->gamblings_id, ['id'=>'gamblings_id','required','class'=>'form-control','onclick'=>'search_sub_gamblings(this.value)']) !!}
            </td>
        </tr>
        <tr>
            <td>Sorteo:</td>
            <td>
                {!! Form::select('sub_gamblings_id',$sub_gamblings_general , $sub_gamblings, ['id'=>'sub_gamblings_id','required','class'=>'form-control ']) !!}
            </td>
        </tr>
        <tr>
            <td>Monto Limite</td>
            <td>
                {!! Form::number('amount',$limitsOnly->amount,array('class' => 'form-control','id'=>'amount','required')) !!}
            </td>
        </tr>
        <tr>
            <td>Pago por Jugadas:</td>
            <td>
                {!! Form::select('awards_id',$awards,$limitsOnly->awards_id ,  ['id'=>'awards_id','class'=>'form-control','required']) !!}
            </td>
        </tr>
    </table>
    <nav class="navbar navbar bg">
        {!! Form::hidden('id',$limitsOnly->id,array('class' => 'form-control','id'=>'id','required')) !!}
        {!! Form::submit('Modificar!',array('class' => 'btn btn-primary','id'=>'update')) !!}
    </nav>

{!! Form::close() !!}
</div>