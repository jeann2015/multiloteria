@extends('layouts.header')
@section('content')

<div class="container">
    <br>
<nav class="navbar navbar bg">
    Modificar Limite General
</nav>
<nav class="navbar navbar bg">
    <a href="{{route('limits')}}" class="btn btn-primary btn-mg active" role="button" aria-pressed="true">Regresar</a>
</nav>
<br>
{!! Form::open(array('url' => 'limits/update')) !!}
    <table  class="table">
        <tr>
            <td>Taquilla:</td>
            <td>
                {!! Form::select('id_taquilla', $bookings, $limits->id_taquilla, ['id'=>'id_taquilla','required','class'=>'form-control']) !!}
            </td>
        </tr>
        <tr>
            <td>Juego:</td>
            <td>
                {!! Form::select('gamblings_id', $gamblings, $limits->gamblings_id, ['id'=>'gamblings_id','required','class'=>'form-control','onclick'=>'search_sub_gamblings(this.value)']) !!}
            </td>
        </tr>
        <tr>
            <td>Sorteo:</td>
            <td>
                {!! Form::select('sub_gamblings_id[]',$sub_gamblings_general , $sub_gamblings, ['id'=>'sub_gamblings_id','required','class'=>'form-control ','multiple'=>'true']) !!}
            </td>
        </tr>
        <tr>
            <td>Monto Limite</td>
            <td>
                {!! Form::number('amount',$limits->amount,array('class' => 'form-control','id'=>'amount','required')) !!}
            </td>
        </tr>
        <tr>
            <td>Pago por Jugadas:</td>
            <td>
                {!! Form::select('awards_id',$awards ,$limits->awards_id, ['id'=>'awards_id','class'=>'form-control','required']) !!}
            </td>
        </tr>
    </table>
    <nav class="navbar navbar bg">
        {!! Form::hidden('id',$limits->id,array('class' => 'form-control','id'=>'id','required')) !!}
        {!! Form::submit('Modificar!',array('class' => 'btn btn-primary','id'=>'update')) !!}
    </nav>
{!! Form::close() !!}
</div>