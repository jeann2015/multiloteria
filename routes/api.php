<?php

use App\Gambling;
use App\SubGambling;
use App\TimeSubGamblings;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/search/sub_gamblings/{gamblings_id}', function (Request $request) {

    $g = new SubGambling;
    $sub_gamblings = $g->get_sub_gamblings_id($request->gamblings_id);

    return response()->json($sub_gamblings);
});


Route::get('/search/sub_gamblings/time/{gamblings_id}/{id_taquilla}', function (Request $request) {
    $g = new Gambling;
    $sub_gamblings = $g->get_sub_gamblings_time($request->gamblings_id,$request->id_taquilla);
    return response()->json($sub_gamblings);
});


Route::get('/search/time_sub_gamblings/time/{sub_gamblings_id}/{id_taquilla}', function (Request $request) {
    $g = new Gambling;
    $sub_gamblings = $g->get_time_sub_gamblings_time($request->sub_gamblings_id,$request->id_taquilla);
    return response()->json($sub_gamblings);

});

Route::get('/search/time_sub_gamblings/all/{sub_gamblings_id}', function (Request $request) {
    $g = new TimeSubGamblings;
    $sub_gamblings = $g->get_time_sub_gamblings_time_all($request->sub_gamblings_id);
    return response()->json($sub_gamblings);

});

Route::get('/search/sub_gamblings/all/{gamblings_id}', function (Request $request) {
    $g = new Gambling;
    $sub_gamblings = $g->get_time_sub_gamblings_time_all($request->gamblings_id);
    return response()->json($sub_gamblings);
});

Route::get('/search/time_sub_gamblings/time/temporal/{sub_gamblings_id}/{id_taquilla}/{id_tickets_temporal}', function (Request $request) {
    $g = new Gambling;
    $sub_gamblings = $g->get_time_sub_gamblings_time_temporal($request->sub_gamblings_id,$request->id_taquilla,$request->id_tickets_temporal);
    return response()->json($sub_gamblings);

});

Route::get('/search/time_sub_gamblings/time/temporal/{sub_gamblings_id}/{id_taquilla}/{id_tickets_temporal}', function (Request $request) {
    $g = new Gambling;
    $sub_gamblings = $g->get_time_sub_gamblings_time_temporal($request->sub_gamblings_id,$request->id_taquilla,$request->id_tickets_temporal);
    return response()->json($sub_gamblings);

});
