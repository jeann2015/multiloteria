<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/user', 'UserController@search')->name('user');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout')->name('logout');

/**Juegos**/
Route::get('/gamblings', 'GamblingController@index')->name('gamblings');

Route::get('/gamblings/add', 'GamblingController@add')->name('gamblings/add');
Route::post('/gamblings/new', 'GamblingController@news')->name('gamblings/news');

Route::get('/gamblings/edit/{id}', 'GamblingController@edit')->name('gamblings/edit');
Route::post('/gamblings/update', 'GamblingController@update')->name('gamblings/update');

Route::get('/gamblings/delete/{id}', 'GamblingController@delete')->name('gamblings/delete');
Route::post('/gamblings/destroy', 'GamblingController@delete')->name('gamblings/destroy');

Route::get('/subgamblings/subadd/{id}', 'GamblingController@subadd')->name('subgamblings/subadd');
Route::post('/subgamblings/subnew', 'GamblingController@subnews')->name('subgamblings/subnew');

Route::get('/subgamblings/subedit/{id}/{subid}', 'GamblingController@subedit')->name('subgamblings/subedit');
Route::post('/subgamblings/subupdate', 'GamblingController@subupdate')->name('subgamblings/subupdate');

Route::get('/subgamblings/subediti/{id}/{subid}', 'GamblingController@subediti')->name('subgamblings/subediti');
Route::post('/subgamblings/subupdatei', 'GamblingController@subupdatei')->name('subgamblings/subupdatei');

Route::get('/subgamblings/subdelete/{id}/{subid}', 'GamblingController@subdelete')->name('subgamblings/subdelete');
Route::post('/subgamblings/subdestroy', 'GamblingController@subdestroy')->name('subgamblings/subdestroy');
/**Juegos**/

/**Limites**/
Route::get('/limits', 'LimitController@index')->name('limits');

Route::get('/limits/add', 'LimitController@add')->name('limits/add');
Route::post('/limits/new', 'LimitController@news')->name('limits/new');

Route::get('/limits/edit/{id}/{gamblings_id}', 'LimitController@edit')->name('limits/edit');
Route::post('/limits/update', 'LimitController@update')->name('limits/update');

Route::get('/limits/editi/{id}', 'LimitController@editi')->name('limits/editi');
Route::post('/limits/updatei', 'LimitController@updatei')->name('limits/updatei');

Route::get('/limits/delete/{id}/{gamblings_id}', 'LimitController@delete')->name('limits/delete');
Route::post('/limits/destroy', 'LimitController@destroy')->name('limits/destroy');
/**Limites**/

/**Taquilla**/
Route::get('/booking', 'BookingController@index')->name('booking');
Route::post('/booking/new', 'BookingController@news')->name('booking/new');
Route::post('/booking/destroy', 'BookingController@destroy')->name('booking/destroy');

Route::get('sales', 'BookingController@sales')->name('sales');

Route::post('/tickets/new', 'TicketsController@news')->name('tickets/new');
Route::get('/tickets/delete/{tickets_id}', 'TicketsController@delete')->name('tickets/delete');
Route::post('/tickets/destroy', 'TicketsController@destroy')->name('tickets/destroy');

Route::get('/tickets/temporals/{id_temporal}', 'BookingController@temporals')->name('tickets/temporals');
Route::get('/tickets/temporals/delete/{id}/{id_temporal}', 'BookingController@delete')->name('tickets/temporals/delete');
/**Taquilla**/

/**Premios por Jugadas**/
Route::get('/awards', 'AwardController@index')->name('awards');

Route::get('/awards/add', 'AwardController@add')->name('awards/add');
Route::post('/awards/new', 'AwardController@news')->name('awards/new');

Route::get('/awards/edit/{id}', 'AwardController@edit')->name('awards/edit');
Route::post('/awards/update', 'AwardController@update')->name('awards/update');

Route::get('/awards/delete/{id}', 'AwardController@delete')->name('awards/delete');
Route::post('/awards/destroy', 'AwardController@destroy')->name('awards/destroy');
/**Premios por Jugadas**/

/**Premiacion**/
Route::get('/rewards', 'RewardController@index')->name('rewards');

Route::get('/rewards/add', 'RewardController@add')->name('rewards/add');
Route::post('/rewards/new', 'RewardController@news')->name('rewards/new');

Route::get('/rewards/edit/{id}', 'RewardController@edit')->name('rewards/edit');
Route::post('/rewards/update', 'RewardController@update')->name('rewards/update');

Route::get('/rewards/delete/{id}', 'RewardController@delete')->name('rewards/delete');
Route::post('/rewards/destroy', 'RewardController@destroy')->name('rewards/destroy');
/**Premiacion**/