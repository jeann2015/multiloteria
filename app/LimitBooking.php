<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LimitBooking extends Model
{
    protected $table="limits_bookings";

    protected $fillable = [
        'id_taquilla','sub_gamblings_id','amount','time_sub_gamblings_id','awards_id'
    ];

    /**
     * @return mixed
     */
    public static function get_limit_booking($id=0)
    {
        if($id=="0") {
            $bookings = DB::select("select lb.id,lb.amount,
            lb.sub_gamblings_id,ta.id_taquilla,ta.nombre,
            sg.description as description,
            g.description as juego,
            t.hora,
            t.gamblings_id,
            lb.time_sub_gamblings_id,
            a.id as awards_id,
            concat('Monto pagado: ',a.amount_play,' A pagar: ',a.amount_pay) as description_awards
            from loteria.taquillas ta ,
            multiloteria.limits_bookings lb,
            multiloteria.sub_gamblings sg,
            multiloteria.gamblings g, 
            multiloteria.time_sub_gamblings t,
            multiloteria.awards a              
            where 
            ta.id_taquilla = lb.id_taquilla AND 
            lb.sub_gamblings_id = sg.id AND 
            lb.time_sub_gamblings_id = t.id AND
            g.id = sg.gamblings_id AND 
            lb.awards_id = a.id        
            ");
        }
        elseif($id<>"0")
            {
                $bookings = DB::select("select lb.id,lb.amount,
                lb.sub_gamblings_id,ta.id_taquilla,ta.nombre,
                sg.description as description,
                g.description as juego,
                t.hora,
                t.gamblings_id,
                lb.time_sub_gamblings_id,
                a.id as awards_id,
                concat('Monto pagado: ',a.amount_play,' A pagar: ',a.amount_pay) as description_awards
                from loteria.taquillas ta ,
                multiloteria.limits_bookings lb,
                multiloteria.sub_gamblings sg,
                multiloteria.gamblings g, 
                multiloteria.time_sub_gamblings t,
                multiloteria.awards a
                where 
                ta.id_taquilla = lb.id_taquilla AND 
                lb.sub_gamblings_id = sg.id AND 
                lb.time_sub_gamblings_id = t.id AND
                g.id = sg.gamblings_id AND
                lb.awards_id = a.id    AND 
                lb.id = $id
                ");

        }

        return $bookings;
    }

    /**
     * @param int $taquilla_id
     * @return mixed
     */
    public static function get_limit_booking_booking($taquilla_id=0,$gamblings_id)
    {
            $bookings = DB::select(" select lb.id,lb.amount,
                lb.sub_gamblings_id,ta.id_taquilla,ta.nombre,
                sg.description as description,
                g.description as juego,
                t.hora,
                t.gamblings_id,
                lb.time_sub_gamblings_id,
                t.id as time_sub_gamblings_id,
                a.id as awards_id,
                concat('Monto pagado: ',a.amount_play,' A pagar: ',a.amount_pay) as description_awards
                from loteria.taquillas ta ,
                multiloteria.limits_bookings lb,
                multiloteria.sub_gamblings sg,
                multiloteria.gamblings g, 
                multiloteria.time_sub_gamblings t ,
                multiloteria.awards a             
                where 
                ta.id_taquilla = lb.id_taquilla AND 
                lb.sub_gamblings_id = sg.id AND 
                lb.time_sub_gamblings_id = t.id AND                
                g.id = sg.gamblings_id AND 
                lb.awards_id = a.id    AND 
                ta.id_taquilla = $taquilla_id AND 
                sg.gamblings_id = $gamblings_id 
                ");

        return $bookings;
    }

    /**
     * @param $taquilla_id
     * @param $sub_time_gamblings_id
     * @param $amount
     * @return bool
     */
    public static function validate_limit_amount_time($taquilla_id,$sub_time_gamblings_id,$amount){

        $LimitBooking = LimitBooking::where('id_taquilla',$taquilla_id)
            ->where('time_sub_gamblings_id',$sub_time_gamblings_id)
            ->where('amount','>=',$amount)
            ->get();

        if($LimitBooking->count()>0){
            $tsg = new TimeSubGamblings;

            $TimeSubGamblings = TimeSubGamblings::where('id',$sub_time_gamblings_id)
                ->where('status','=','1')
                ->where('hora','>=',$tsg->get_hora_current())
                ->get();

            if($TimeSubGamblings->count()>0){
                return true;
            }else{
                return false;
            }

        }else{
            return false;
        }
    }


}
