<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LimitUser extends Model
{
    protected $table="limits_users";

    protected $fillable = [
        'users_id','sub_gamblings_id','amount'
    ];
}
