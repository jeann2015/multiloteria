<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SubGambling extends Model
{
    protected $table="sub_gamblings";

    protected $fillable = [
        'gamblings_id','hora','description','status','images'
    ];

    /**
     * @param $gamblings_id
     * @param $sub_gamblings_id
     * @return array
     */
    public function horas_gamblings_id($gamblings_id,$sub_gamblings_id)
    {
        $subgamblings = TimeSubGamblings::where('gamblings_id',$gamblings_id)
            ->where('status',1)
            ->where('sub_gamblings_id',$sub_gamblings_id)
            ->select('hora','id')->get();
        $hora=array();

        foreach ($subgamblings as $subgambling){
            $hora[Carbon::parse($subgambling->hora)->format('h:i A')] = Carbon::parse($subgambling->hora)->format('h:i A');
        }
        return $hora;
    }

    public function horas_gamblings_id_i($gamblings_id,$sub_gamblings_id)
    {
        $hora=array();

        $subgamblings = TimeSubGamblings::where('gamblings_id',$gamblings_id)
            ->where('sub_gamblings_id',$sub_gamblings_id)
            ->where('status',1)
            ->pluck('hora','hora');

        foreach ($subgamblings as $subgambling){
            $hora[Carbon::parse($subgambling)->format('h:i A')] = Carbon::parse($subgambling)->format('h:i A');
        }

        return $hora;
    }

    /**
     * @param $gamblings_id
     * @return mixed
     */
    public function get_sub_gamblings_id($gamblings_id){

        $sub_gamblings = \DB::table('time_sub_gamblings')
            ->select(
                'time_sub_gamblings.id',
                \DB::raw("concat(sub_gamblings.description,' ',TIME_FORMAT(time_sub_gamblings.hora, '%h:%i %p')) 
                as sub_gamblings_description")
            )
            ->join('sub_gamblings','time_sub_gamblings.sub_gamblings_id','sub_gamblings.id')
            ->where('time_sub_gamblings.gamblings_id','=',$gamblings_id)
            ->where('time_sub_gamblings.status',1)
            ->pluck('time_sub_gamblings.sub_gamblings_description', 'time_sub_gamblings.id');

        return $sub_gamblings;
    }

    public function get_sub_gamblings_temporal_id($gamblings_id,$id_tickets_temporal){

        $sub_gamblings = \DB::table('time_sub_gamblings')
            ->select(
                'time_sub_gamblings.id',
                \DB::raw("concat(sub_gamblings.description,' ',TIME_FORMAT(time_sub_gamblings.hora, '%h:%i %p')) 
                as sub_gamblings_description")
            )
            ->join('sub_gamblings','time_sub_gamblings.sub_gamblings_id','sub_gamblings.id')
            ->where('time_sub_gamblings.gamblings_id','=',$gamblings_id)
            ->where('time_sub_gamblings.status',1)
            ->pluck('time_sub_gamblings.sub_gamblings_description', 'time_sub_gamblings.id');

        return $sub_gamblings;
    }

}
