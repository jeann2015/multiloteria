<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketsTemporal extends Model
{
    protected $table="tickets_temporals";

    protected $fillable = [
        'users_id','sub_gamblings_id','amount','id_taquilla','hora_sub_gamblings','gamblings_id','time_sub_gamblings_id','id_tickets_number'
    ];
}
