<?php

namespace App\Http\Controllers;

use App\Sessiones;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $s = new Sessiones;
        if($s->search_session()){
            return view('home');
        }
        else{
            return view('error');
        }
    }

    public function logout(Request $request){
        $request->session()->flush();
        return view('logout');
    }
}
