<?php

namespace App\Http\Controllers;

use App\Gambling;
use App\IdTickets;
use App\LimitBooking;
use App\Sessiones;
use App\Tickets;
use App\TicketsTemporal;
use App\TimeSubGamblings;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BookingController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {

            $g = new Gambling;
            $gamblings = $g->get_gamblings_time();
            $taquillas_id = Session::get('taquillas_id');
            $id_temporal = Session::get('id_temporal');
            $messages = Session::get('messages');

            if(isset($id_temporal) && $id_temporal<>"" && $id_temporal<>"0"){

                $TicketsTemporal = TicketsTemporal::
                join('gamblings','tickets_temporals.gamblings_id','gamblings.id')
                ->join('sub_gamblings','tickets_temporals.sub_gamblings_id','sub_gamblings.id')
                ->join('time_sub_gamblings','tickets_temporals.time_sub_gamblings_id','time_sub_gamblings.id')
                ->where('tickets_temporals.id_tickets_number',$id_temporal)
                    ->select(
                        'tickets_temporals.amount',
                        'tickets_temporals.id_tickets_number',
                        \DB::raw("concat('Juego:',gamblings.description,'->Sorteo:',sub_gamblings.description,'->Hora:',
                        TIME_FORMAT(time_sub_gamblings.hora, '%h:%i %p'),'->Monto Jugado:',tickets_temporals.amount)
                as sub_gamblings_description"
                        )
                    )->get();
                $request->session()->forget('messages');
                return view('booking.index', compact('gamblings', 'taquillas_id','TicketsTemporal','messages'));
            }
            elseif(isset($d_temporal) && $id_temporal<>"" && $id_temporal=="0" ){
                $request->session()->forget('id_temporal');
                $request->session()->forget('messages');
                return view('booking.index', compact('gamblings', 'taquillas_id','messages'));
            }
            else {
                $request->session()->forget('id_temporal');
                $request->session()->forget('messages');
                return view('booking.index', compact('gamblings', 'taquillas_id','messages'));
            }

        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function news(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {

            $this->validate($request, [
                'gamblings_id' => 'required:numeric',
                'sub_gamblings_id' => 'required:numeric',
                'time_sub_gamblings_id'=>'required:numeric',
                'amount' => 'required:numeric',
            ]);

            $taquillas_id = Session::get('taquillas_id');
            $users_id = Session::get('users_id');
            $l = new LimitBooking;
            $value_limit=true;

            if($request->id_tickets_temporal=="") {

                $IdTickets = IdTickets::get_id_tickets_temporals();

                foreach ($request->time_sub_gamblings_id as $time_sub_gamblings_id) {
                    $value_limit = $l->validate_limit_amount_time($taquillas_id ,$time_sub_gamblings_id,$request->amount);
                    if($value_limit==false){
                        $value_limit=false;
                        $messages="Limite No permitido";
                    }
                }

                if($value_limit==true) {
                    foreach ($request->time_sub_gamblings_id as $time_sub_gamblings_id) {

                        $awards_amount = LimitBooking::join('awards', 'limits_bookings.awards_id', 'awards.id')
                            ->select('awards.amount_play', 'awards.amount_pay')
                            ->where('limits_bookings.time_sub_gamblings_id', $time_sub_gamblings_id)
                            ->get();

                        $value = $request->amount % $awards_amount->pluck('amount_play')[0];

                        if ($value_limit == true && $value<>0) {
                            $value_limit = false;
                            $messages="No es Multiplo de: ".$awards_amount->pluck('amount_play')[0];
                        }
                    }
                }

                if($value_limit==true) {

                    foreach ($request->time_sub_gamblings_id as $time_sub_gamblings_id) {
                        $TimeSubGamblings = TimeSubGamblings::find($time_sub_gamblings_id);
                        TicketsTemporal::create([
                            'id_taquilla' => $taquillas_id,
                            'gamblings_id' => $request->gamblings_id,
                            'sub_gamblings_id' => $request->sub_gamblings_id,
                            'time_sub_gamblings_id' => $time_sub_gamblings_id,
                            'hora_sub_gamblings' => $TimeSubGamblings->hora,
                            'id_tickets_number' => $IdTickets,
                            'amount' => $request->amount,
                            'users_id' => $users_id
                        ]);
                    }
                    $messages="Jugada Agregada";
                    $request->session()->put('id_temporal', $IdTickets);
                    $request->session()->put('messages', $messages);
                }else{

                    $request->session()->put('id_temporal', 0);
                    $request->session()->put('messages', $messages);
                    return redirect()->route('booking');
                }

            }else{

                foreach ($request->time_sub_gamblings_id as $time_sub_gamblings_id) {
                    $value_limit = $l->validate_limit_amount_time($taquillas_id ,$time_sub_gamblings_id,$request->amount);

                    if($value_limit==false){
                        $value_limit=false;
                    }
                }

                if($value_limit==true) {
                    foreach ($request->time_sub_gamblings_id as $time_sub_gamblings_id) {

                        $awards_amount = LimitBooking::join('awards', 'limits_bookings.awards_id', 'awards.id')
                            ->select('awards.amount_play', 'awards.amount_pay')
                            ->where('limits_bookings.time_sub_gamblings_id', $time_sub_gamblings_id)
                            ->get();

                        $value = $request->amount % $awards_amount->pluck('amount_play')[0];

                        if ($value_limit == true && $value<>0) {
                            $value_limit = false;
                            $messages="No es Multiplo de: ".$awards_amount->pluck('amount_play')[0];
                        }
                    }
                }

                if($value_limit==true) {
                    foreach ($request->time_sub_gamblings_id as $time_sub_gamblings_id) {
                        $TimeSubGamblings = TimeSubGamblings::find($time_sub_gamblings_id);
                        TicketsTemporal::create([
                            'id_taquilla' => $taquillas_id,
                            'gamblings_id' => $request->gamblings_id,
                            'sub_gamblings_id' => $request->sub_gamblings_id,
                            'time_sub_gamblings_id' => $time_sub_gamblings_id,
                            'hora_sub_gamblings' => $TimeSubGamblings->hora,
                            'id_tickets_number' => $request->id_tickets_temporal,
                            'amount' => $request->amount,
                            'users_id' => $users_id
                        ]);
                    }
                    $messages="Jugada Agregada";
                    $IdTickets = $request->id_tickets_temporal;
                    $request->session()->put('id_temporal', $IdTickets);
                    $request->session()->put('messages', $messages);

                }else{
                    $IdTickets = $request->id_tickets_temporal;
                    $request->session()->put('id_temporal', $IdTickets);
                    $request->session()->put('messages', $messages);
                    return redirect()->route('booking');
                }

            }

            return redirect()->route('booking');

        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function destroy(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {

            $this->validate($request, [
                'id' => 'required:numeric|exists:tickets_temporals,id_tickets_number'
            ]);

            $request->session()->forget('id_temporal');
            $messages="Jugada Eliminada por Completo";
            $request->session()->put('messages', $messages);

            TicketsTemporal::where('id_tickets_number',$request->id)->delete();
            return redirect()->route('booking');

        } else {
            return view('error');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sales(Request $request)
    {
        $s = new Sessiones;


        if ($s->search_session()) {

            $tsg = new TimeSubGamblings;
            $g = new Gambling;
            $gamblings = $g->get_gamblings_time();
            $taquillas_id = Session::get('taquillas_id');
            $date_actual=$tsg->get_date_current();

            $tickets = Tickets::
            join('gamblings','tickets.gamblings_id','gamblings.id')
                ->join('sub_gamblings','tickets.sub_gamblings_id','sub_gamblings.id')
                ->join('time_sub_gamblings','tickets.time_sub_gamblings_id','time_sub_gamblings.id')
                ->where('tickets.id_taquilla',$taquillas_id)
                ->whereBetween('tickets.created_at',[$date_actual." 00:00:00",$date_actual." 23:59:59"])
                ->select(
                    'tickets.id_tickets_number',
                    'tickets.created_at',
                    'tickets.amount',
                    'tickets.id_tickets_number',
                    \DB::raw("concat('Juego:',gamblings.description,'->Sorteo:',sub_gamblings.description,'->Hora:',
                    TIME_FORMAT(time_sub_gamblings.hora, '%h:%i %p'),'->Monto Jugado:',tickets.amount) as sub_gamblings_description"
                    )
                )
                ->orderbyRaw('id_tickets_number desc')
                ->get();

            $messages = Session::get('messages');
            $request->session()->forget('messages');

            return view('booking.sales', compact('gamblings', 'taquillas_id','tickets','messages'));

        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function temporals(Request $request)
    {
        $TicketsTemporal = TicketsTemporal::
        join('gamblings','tickets_temporals.gamblings_id','gamblings.id')
            ->join('sub_gamblings','tickets_temporals.sub_gamblings_id','sub_gamblings.id')
            ->join('time_sub_gamblings','tickets_temporals.time_sub_gamblings_id','time_sub_gamblings.id')
            ->join('limits_bookings','limits_bookings.time_sub_gamblings_id','time_sub_gamblings.id')
            ->join('awards','limits_bookings.awards_id','awards.id')
            ->where('tickets_temporals.id_tickets_number',$request->id_temporal)
            ->select(
                'tickets_temporals.id',
                'tickets_temporals.amount',
                'tickets_temporals.id_tickets_number',
                \DB::raw("concat('Juego:',gamblings.description,'->Sorteo:',sub_gamblings.description,'->Hora:',
                        TIME_FORMAT(time_sub_gamblings.hora, '%h:%i %p'),'->Monto Jugado:',tickets_temporals.amount,
                        ' x ',awards.amount_pay,' = ', format(awards.amount_pay*tickets_temporals.amount,2))
                as sub_gamblings_description"),
                \DB::raw("awards.amount_pay*tickets_temporals.amount as amount_pay_play")

            )->get();

        return view('booking.temporals', compact('TicketsTemporal'));
    }

    public function delete(Request $request)
    {
        TicketsTemporal::where('id',$request->id)->delete();

        return redirect()->route('tickets/temporals', ['id_temporal' => $request->id_temporal,'messages'=>'Juagada Eliminada']);
    }



}
