<?php

namespace App\Http\Controllers;

use App\Gambling;
use App\LimitBooking;
use App\Sessiones;
use App\SubGambling;
use App\TimeSubGamblings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class GamblingController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            $gamblings = Gambling::all();
            return view('gamblings.index', compact('gamblings'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            return view('gamblings.add');
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subadd(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
            $gamblings = Gambling::find($request->id);
            $hora = $gamblings->horas();
            return view('gamblings.subadd', compact('gamblings', 'hora'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function news(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
            $users_id = Session::get('users_id');

            Gambling::create([
                'description'=>$request->description,
                'users_id' => $users_id
            ]);

            return redirect('gamblings');
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function subnews(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'id' => 'required:numeric|exists:gamblings,id',
            'hora' => 'required',
            'images1' => 'required:image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $s = new Sessiones;
        if ($s->search_session()) {
            if ($request->file('images1')) {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile = $link . '.jpg';
                $images1 = \Image::make($request->file('images1')->getRealPath())
                    ->save($pathToFile);
            } else {
                $images1="";
            }



            $subgam = SubGambling::create([
                'gamblings_id' => $request->id,
                'description' => $request->description,
                'status'=>1,
                'images'=>$images1
            ]);

            Storage::delete($pathToFile);

            foreach ($request->hora as $horas) {
                $hora = date("H:i:s", strtotime($horas));

                TimeSubGamblings::create([
                    'gamblings_id' => $request->id,
                    'hora' => $hora,
                    'sub_gamblings_id' => $subgam->id,
                    'status'=>1
                ]);
            }

            return redirect('gamblings/edit/'.$request->id);
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
            $gamblings = Gambling::find($request->id);
            $subgamblings = SubGambling::where('gamblings_id', $request->id)->get();
            return view('gamblings.mod', compact('gamblings', 'subgamblings'));
        } else {
            return view('error');
        }
    }

    public function subedit(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
            $gamblings = Gambling::find($request->id);
            $hora = $gamblings->horas();

            $subgamblings = SubGambling::find($request->subid);



            if ($subgamblings->images <> "") {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile[1] = $link . '.jpg';
                \Image::make($subgamblings->images, 100, 100)->save($pathToFile[1]);
            } else {
                $pathToFile[1] = "";
            }

            $horas = $subgamblings->horas_gamblings_id($request->id, $request->subid);

            return view('gamblings.submod', compact('gamblings', 'subgamblings', 'hora', 'horas', 'pathToFile'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subediti(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
            $gamblings = Gambling::find($request->id);
            $hora = $gamblings->horas();

            $subgamblings = SubGambling::find($request->subid);
            $horas = $subgamblings->horas_gamblings_id_i($request->id, $request->subid);

            return view('gamblings.submodi', compact('gamblings', 'subgamblings', 'hora', 'horas'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function update(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
            $users_id = Session::get('users_id');
            $gamblings = Gambling::find($request->id);
            $gamblings->description = $request->description;
            $gamblings->users_id = $users_id;
            $gamblings->save();

            return redirect('gamblings');
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function subupdate(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'id' => 'required:numeric|exists:gamblings,id',
            'subid' => 'required:numeric|exists:sub_gamblings,id',
            'hora' => 'required',
        ]);

        $s = new Sessiones;
        if ($s->search_session()) {
            if ($request->file('images1')) {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile = $link . '.jpg';
                $images1 = \Image::make($request->file('images1')->getRealPath())
                    ->save($pathToFile);

                SubGambling::where('gamblings_id', $request->id)
                    ->where('id', $request->subid)
                    ->update([
                        'description' => $request->description,
                        'status'=>  1,
                        'images'=>$images1
                    ]);
            } else {
                SubGambling::where('gamblings_id', $request->id)
                    ->where('id', $request->subid)
                    ->update([
                        'description' => $request->description,
                        'status'=>  1
                    ]);
            }

            TimeSubGamblings::where('gamblings_id', $request->id)
                ->where('sub_gamblings_id', $request->subid)
                ->update(['status'=>0]);

            foreach ($request->hora as $horas) {
                $hora = date("H:i:s", strtotime($horas));

                $timesubgamblings = TimeSubGamblings::where('gamblings_id', $request->id)
                    ->where('sub_gamblings_id', $request->subid)
                    ->where('hora', $hora)
                    ->get();

                if ($timesubgamblings->count()>0) {
                    foreach ($timesubgamblings as $timesubgambling) {
                        TimeSubGamblings::where('id', $timesubgambling->id)
                        ->update(['hora' => $hora, 'status'=>1]);
                    }

                    $limitTaquillas = LimitBooking::select('id_taquilla', 'awards_id', 'amount')
                        ->where('sub_gamblings_id', $request->subid)
                        ->get();

                    foreach ($limitTaquillas as $limitTaquilla) {
                        $limitTaquillas = LimitBooking::select('id_taquilla', 'awards_id', 'amount')
                            ->where('time_sub_gamblings_id', $timesubgambling->id)
                            ->where('id_taquilla', $limitTaquilla->id_taquilla)
                            ->get();

                        if ($limitTaquillas->count()==0) {
                            LimitBooking::create([
                                'sub_gamblings_id' => $request->subid,
                                'time_sub_gamblings_id' => $timesubgambling->id,
                                'id_taquilla' => $limitTaquilla->id_taquilla,
                                'awards_id' => $limitTaquilla->awards_id,
                                'amount' => $limitTaquilla->amount,
                            ]);
                        }
                    }
                } else {
                    $limitTaquillas = LimitBooking::select('id_taquilla', 'awards_id', 'amount', 'time_sub_gamblings_id')
                        ->where('sub_gamblings_id', $request->subid)
                        ->limit(1)
                        ->get();

                    $TimeSubGamblings = TimeSubGamblings::create(['hora' => $hora,
                        'sub_gamblings_id'=>$request->subid,
                        'gamblings_id'=>$request->id,
                        'status'=>1]);

                    foreach ($limitTaquillas as $limitTaquilla) {
                        LimitBooking::create([
                                'sub_gamblings_id' => $request->subid,
                                'time_sub_gamblings_id' => $TimeSubGamblings->id,
                                'id_taquilla' => $limitTaquilla->id_taquilla,
                                'awards_id' => $limitTaquilla->awards_id,
                                'amount' => $limitTaquilla->amount,
                            ]);
                    }
                }
            }

            return redirect('gamblings/edit/'.$request->id);
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function subupdatei(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'id' => 'required:numeric',
            'subid' => 'required:numeric',
            'hora' => 'required',
            'status'=>'required:numeric'
        ]);

        $s = new Sessiones;

        if ($s->search_session()) {
            $hora_24 = date("H:i:s", strtotime($request->hora));
            SubGambling::where('id', $request->subid)
            ->update([
                'description' => $request->description,
                'hora' => $hora_24,
                'status'=>  $request->status
            ]);

            return redirect('gamblings/edit/'.$request->id);
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
            $gamblings = Gambling::find($request->id);
            return view('gamblings.del', compact('gamblings'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subdelete(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
            $gamblings = Gambling::find($request->id);
            $hora = $gamblings->horas();

            $subgamblings = SubGambling::find($request->subid);
            $horas = $subgamblings->horas_gamblings_id($request->id, $request->subid);

            return view('gamblings.subdel', compact('gamblings', 'subgamblings', 'hora', 'horas'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function destroy(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
            Gambling::find($request->id)->delete();
            return redirect('gamblings');
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function subdestroy(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {
//            SubGambling::find($request->subid)->delete();
            return redirect('gamblings/edit/'.$request->id);
        } else {
            return view('error');
        }
    }
}
