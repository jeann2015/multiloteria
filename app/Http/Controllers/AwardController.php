<?php

namespace App\Http\Controllers;

use App\Award;
use App\Sessiones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AwardController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            $awards = Award::all();
            return view('awards.index', compact('awards'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            return view('awards.add');
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function news(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {

            Award::create([
                'description'=>$request->description,
                'amount_play' => $request->amount_play,
                'amount_pay' => $request->amount_pay
            ]);

            return redirect('awards');

        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {

            $awards = Award::find($request->id);
            return view('awards.mod',compact('awards'));

        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function update(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {

            Award::where('id',$request->id)->update([
                'description'=>$request->description,
                'amount_play' => $request->amount_play,
                'amount_pay' => $request->amount_pay
            ]);

            return redirect('awards');

        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {

            $awards = Award::find($request->id);
            return view('awards.del',compact('awards'));

        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function destroy(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {

            return redirect('awards');

        } else {
            return view('error');
        }
    }
}
