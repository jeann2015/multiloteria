<?php

namespace App\Http\Controllers;

use App\Award;
use App\Booking;
use App\Gambling;
use App\LimitBooking;
use App\Sessiones;
use App\SubGambling;
use App\TimeSubGamblings;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LimitController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            $limits = LimitBooking::get_limit_booking();
            return view('limit.index', compact('limits'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            $gambling = Gambling::pluck('description', 'id')->put('0', 'Todos los Juegos');
            $gamblings = array_sort_recursive($gambling->toArray());

            $booking = Booking::pluck('nombre', 'id_taquilla')->put('0', 'Todos las Taquillas');
            $bookings = array_sort_recursive($booking->toArray());

            $award = Award::select(
                'awards.id',
                \DB::raw("concat(awards.description,' ',awards.amount_play,'-> ',awards.amount_pay) 
                as description")
            )->pluck('description', 'id')->put('0', 'Seleccione un Premio por Jugada');
            $awards = array_sort_recursive($award->toArray());

            return view('limit.add', compact('gamblings', 'bookings','awards'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'id_taquilla' => 'required:numeric',
            'gamblings_id' => 'required:numeric',
            'amount' => 'required:numeric',
            'awards_id'=>'required:numeric|exists:awards,id'
        ]);

        $s = new Sessiones;

        if ($s->search_session()) {
            if ($request->id_taquilla<>0) {
                foreach ($request->sub_gamblings_id as $sub_gamblings_i) {
                    $TimeSubGamblings = TimeSubGamblings::where('id', $sub_gamblings_i)->get();

                    LimitBooking::create([
                        'sub_gamblings_id' => $TimeSubGamblings->pluck('sub_gamblings_id')[0],
                        'id_taquilla' => $request->id_taquilla,
                        'amount' => $request->amount,
                        'time_sub_gamblings_id' => $sub_gamblings_i,
                        'awards_id'=>$request->awards_id
                    ]);
                }
            } else {
                $bookings = Booking::where('inactivo', 0)->get();
                $TimeSubGamblings = TimeSubGamblings::where('status', 1)->get();

                foreach ($bookings as $booking) {
                    foreach ($TimeSubGamblings as $TimeSubGambling) {
                        LimitBooking::create([
                            'sub_gamblings_id' => $TimeSubGambling->sub_gamblings_id,
                            'id_taquilla' => $booking->id_taquilla,
                            'time_sub_gamblings_id' => $TimeSubGambling->id,
                            'amount' => $request->amount,
                            'awards_id'=>$request->awards_id
                        ]);
                    }
                }
            }

            return redirect('limits');
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $s = new Sessiones;
        $sg = new SubGambling;

        if ($s->search_session()) {
            $gambling = Gambling::pluck('description', 'id')->put('0', 'Todos los Juegos');
            $gamblings = array_sort_recursive($gambling->toArray());

            $booking = Booking::pluck('nombre', 'id_taquilla')->put('0', 'Todos las Taquillas');
            $bookings = array_sort_recursive($booking->toArray());

            $award = Award::select(
                'awards.id',
                \DB::raw("concat(awards.description,' ',awards.amount_play,'-> ',awards.amount_pay) 
                as description")
            );
            $awards = array_sort_recursive($award->toArray());

            $limits = LimitBooking::get_limit_booking_booking($request->id,$request->gamblings_id);

            foreach ($limits as $limit) {
                $sub_gamblings[$limit->description." ".Carbon::parse($limit->hora)->format('h:i A')]=$limit->time_sub_gamblings_id;
                $sub_gamblings_general = $sg->get_sub_gamblings_id($limit->gamblings_id);
            }

            $limits = array_first($limits, function ($key) {
                return $key;
            });

            return view('limit.mod', compact('awards','gamblings', 'bookings', 'limits', 'sub_gamblings', 'sub_gamblings_general'));

        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id_taquilla' => 'required:numeric',
            'gamblings_id' => 'required:numeric',
            'sub_gamblings_id' => 'required:numeric',
            'amount' => 'required:numeric',
        ]);

        $s = new Sessiones;

        if ($s->search_session()) {

            LimitBooking::where('id_taquilla', $request->id_taquilla)
                ->delete();

            foreach ($request->sub_gamblings_id as $sub_gamblings_i) {
                $TimeSubGamblings = TimeSubGamblings::where('id', $sub_gamblings_i)->get();
                foreach ($TimeSubGamblings as $TimeSubGambling) {
                    LimitBooking::create([
                        'sub_gamblings_id' => $TimeSubGambling->sub_gamblings_id,
                        'id_taquilla' => $request->id_taquilla,
                        'amount' => $request->amount,
                        'time_sub_gamblings_id' => $sub_gamblings_i,
                        'awards_id'=>$request->awards_id
                    ]);
                }
            }

            return redirect('limits');
        } else {
            return view('error');
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editi(Request $request)
    {
        $s = new Sessiones;
        $sg = new SubGambling;


        if ($s->search_session()) {
            $gambling = Gambling::pluck('description', 'id')->put('0', 'Todos los Juegos');
            $gamblings = array_sort_recursive($gambling->toArray());

            $booking = Booking::pluck('nombre', 'id_taquilla')->put('0', 'Todos las Taquillas');
            $bookings = array_sort_recursive($booking->toArray());

            $award = Award::select(
                'awards.id',
                \DB::raw("concat(awards.description,' ',awards.amount_play,'-> ',awards.amount_pay) 
                as description")
            )->pluck('description', 'id')->put('0', 'Seleccione un Premio por Jugada');
            $awards = array_sort_recursive($award->toArray());

            $limits = LimitBooking::get_limit_booking($request->id);

            foreach ($limits as $limit) {
                $sub_gamblings[$limit->nombre." ".Carbon::parse($limit->hora)->format('h:i A')]=$limit->time_sub_gamblings_id;
                $sub_gamblings_general = $sg->get_sub_gamblings_id($limit->gamblings_id);
            }

            $limitsOnly = array_first($limits, function ($key) {
                return $key;
            });

            return view('limit.modi', compact('awards','gamblings', 'bookings', 'limitsOnly', 'sub_gamblings', 'sub_gamblings_general'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function updatei(Request $request)
    {
        $this->validate($request, [
            'id_taquilla' => 'required:numeric',
            'gamblings_id' => 'required:numeric',
            'sub_gamblings_id' => 'required:numeric',
            'amount' => 'required:numeric',
        ]);

        $s = new Sessiones;

        if ($s->search_session()) {
            LimitBooking::where('id', $request->id)
                ->update([
                    'amount' => $request->amount,
                    'awards_id'=>$request->awards_id
                ]);

            return redirect('limits');
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $s = new Sessiones;
        $sg = new SubGambling;

        if ($s->search_session()) {
            $gambling = Gambling::pluck('description', 'id')->put('0', 'Todos los Juegos');
            $gamblings = array_sort_recursive($gambling->toArray());

            $booking = Booking::pluck('nombre', 'id_taquilla')->put('0', 'Todos las Taquillas');
            $bookings = array_sort_recursive($booking->toArray());

            $limits = LimitBooking::get_limit_booking_booking($request->id,$request->gamblings_id);

            foreach ($limits as $limit) {
                $sub_gamblings[$limit->description." ".Carbon::parse($limit->hora)->format('h:i A')]=$limit->time_sub_gamblings_id;
                $sub_gamblings_general = $sg->get_sub_gamblings_id($limit->gamblings_id);
            }

            $limits = array_first($limits, function ($key) {
                return $key;
            });

            return view('limit.del', compact('gamblings', 'bookings', 'limits', 'sub_gamblings', 'sub_gamblings_general'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id_taquilla' => 'required:numeric',
            'gamblings_id' => 'required:numeric',
            'sub_gamblings_id' => 'required:numeric',
            'amount' => 'required:numeric',
        ]);

        $s = new Sessiones;

        if ($s->search_session()) {
            return redirect('limits');
        } else {
            return view('error');
        }
    }
}
