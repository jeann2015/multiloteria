<?php

namespace App\Http\Controllers;

use App\Gambling;
use App\IdTickets;
use App\Sessiones;
use App\TicketDeleted;
use App\Tickets;
use App\TicketsTemporal;
use App\TimeSubGamblings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class TicketsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function news(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            $id_temporal = Session::get('id_temporal');

            if (isset($id_temporal) && $id_temporal<>"" && $id_temporal<>"0") {
                $TicketsTemporals = TicketsTemporal::where('id_tickets_number', $id_temporal)->get();
                $IdTickets = IdTickets::get_id_tickets();

                $hash_serial = Hash::make(crypt($IdTickets,Hash::make($IdTickets)));

                foreach ($TicketsTemporals as $TicketsTemporal) {

                    Tickets::create([
                        'id_taquilla' => $TicketsTemporal->id_taquilla,
                        'gamblings_id' => $TicketsTemporal->gamblings_id,
                        'sub_gamblings_id' => $TicketsTemporal->sub_gamblings_id,
                        'time_sub_gamblings_id' => $TicketsTemporal->time_sub_gamblings_id,
                        'hora_sub_gamblings' => $TicketsTemporal->hora_sub_gamblings,
                        'id_tickets_number' => $IdTickets,
                        'amount' => $TicketsTemporal->amount,
                        'users_id' => $TicketsTemporal->users_id,
                        'serial'=>$hash_serial
                    ]);
                }

                TicketsTemporal::where('id_tickets_number', $id_temporal)->delete();
                $request->session()->forget('id_temporal');
                $messages = "Ticket Realizado";
                $request->session()->put('messages', $messages);
                return redirect('booking');
            }
        } else {
            return view('error');
        }
    }

    public function delete(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            $tsg = new TimeSubGamblings;
            $g = new Gambling;
            $gamblings = $g->get_gamblings_time();
            $taquillas_id = Session::get('taquillas_id');
            $date_actual=$tsg->get_date_current();

            $tickets = Tickets::
            join('gamblings', 'tickets.gamblings_id', 'gamblings.id')
                ->join('sub_gamblings', 'tickets.sub_gamblings_id', 'sub_gamblings.id')
                ->join('time_sub_gamblings', 'tickets.time_sub_gamblings_id', 'time_sub_gamblings.id')
                ->where('tickets.id_taquilla', $taquillas_id)
                ->where('tickets.id_tickets_number', $request->tickets_id)
                ->whereBetween('tickets.created_at', [$date_actual." 00:00:00",$date_actual." 23:59:59"])
                ->select(
                    'tickets.id_tickets_number',
                    'tickets.created_at',
                    'tickets.amount',
                    'tickets.id_tickets_number',
                    'tickets.time_sub_gamblings_id',
                    \DB::raw("concat('Juego:',gamblings.description,'->Sorteo:',sub_gamblings.description,'->Hora:',
                    TIME_FORMAT(time_sub_gamblings.hora, '%h:%i %p'),'->Monto Jugado:',tickets.amount) as sub_gamblings_description"
                    )
                )
                ->orderbyRaw('id_tickets_number desc')
                ->get();
            $result=true;
            foreach ($tickets as $ticket) {
                $hora = new TimeSubGamblings;
                $TimeSubGamblings = TimeSubGamblings::where('id', $ticket->time_sub_gamblings_id)
                    ->where('hora', '>=', $hora->get_hora_current())
                    ->get();

                if ($result==true && $TimeSubGamblings->count()==0) {
                    $messages="Este ticket no se puede anular";
                    $result=false;
                }
            }
            if ($result==true) {
                return view('booking.del', compact('gamblings', 'taquillas_id', 'tickets'));
            } else {
                return view('booking.del', compact('gamblings', 'taquillas_id', 'tickets', 'messages'));
            }
        } else {
            return view('error');
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function destroy(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            $this->validate($request, [
                'id' => 'required:numeric|exists:tickets,id_tickets_number'
            ]);

            $Tickets = Tickets::where('id_tickets_number', $request->id)->get();
            $users_id = Session::get('users_id');

            $result=true;
            foreach ($Tickets as $ticket) {
                $hora = new TimeSubGamblings;
                $TimeSubGamblings = TimeSubGamblings::where('id', $ticket->time_sub_gamblings_id)
                    ->where('hora', '>=', $hora->get_hora_current())
                    ->get();

                if ($result==true && $TimeSubGamblings->count()==0) {
                    $messages="Este ticket no se puede anular";
                    $result=false;
                }
            }
            if ($result==true) {
                foreach ($Tickets as $Ticket) {
                    TicketDeleted::create([
                        'id_taquilla' => $Ticket->id_taquilla,
                        'gamblings_id' => $Ticket->gamblings_id,
                        'sub_gamblings_id' => $Ticket->sub_gamblings_id,
                        'time_sub_gamblings_id' => $Ticket->time_sub_gamblings_id,
                        'hora_sub_gamblings' => $Ticket->hora_sub_gamblings,
                        'id_tickets_number' => $request->id,
                        'amount' => $Ticket->amount,
                        'users_id' => $users_id
                    ]);
                }

                $Tickets = Tickets::where('id_tickets_number', $request->id)->delete();
                $messages="Ticket: ".$result->id." Fue Eliminado!";
                $request->session()->put('messages', $messages);
                return redirect()->route('sales');
            } else {
                return redirect()->route('sales');
            }
        } else {
            return view('error');
        }
    }
}
