<?php

namespace App\Http\Controllers;

use App\Gambling;
use App\Reward;
use App\Sessiones;
use App\Tickets;
use App\TicketsWinning;
use App\TimeSubGamblings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RewardController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            $rewards = Reward::all();
            return view('rewards.index', compact('rewards'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $s = new Sessiones;

        if ($s->search_session()) {
            $g = new Gambling;
            $gamblings = $g->get_gamblings_time_all();
            return view('rewards.add',compact('gamblings'));
        } else {
            return view('error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function news(Request $request)
    {
        $s = new Sessiones;
        if ($s->search_session()) {

            $tsg = new TimeSubGamblings;
            $date_actual=$tsg->get_date_current();
            $users_id = Session::get('users_id');


            $Tickets = Tickets::where('time_sub_gamblings_id', $request->time_sub_gamblings_id)
                ->whereBetween('tickets.created_at',[$date_actual." 00:00:00",$date_actual." 23:59:59"])
                ->get();


            foreach ($Tickets as $Ticket) {
                TicketsWinning::create([
                    'id_taquilla' => $Ticket->id_taquilla,
                    'gamblings_id' => $Ticket->gamblings_id,
                    'sub_gamblings_id' => $Ticket->sub_gamblings_id,
                    'time_sub_gamblings_id' => $Ticket->time_sub_gamblings_id,
                    'hora_sub_gamblings' => $Ticket->hora_sub_gamblings,
                    'id_tickets_number' => $Ticket->id_tickets_number,
                    'amount' => $Ticket->amount,
                    'users_id' => $Ticket->users_id,
                    'serial'=>$Ticket->serial
                ]);
            }

            Reward::create([
                'time_sub_gamblings_id' => $request->time_sub_gamblings_id,
                'users_id' => $users_id
            ]);

            return redirect('rewards');
        } else {
            return view('error');
        }
    }


}
