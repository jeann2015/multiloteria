<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function search(Request $request)
    {
        $this->validate($request, [
            'users_id' => 'required:numeric',
            'taquillas_id'=>'required:numeric'
        ]);

        Session::flush();
        Session::regenerate();

        $request->session()->put('users_id', $request->users_id);
        $request->session()->put('taquillas_id', $request->taquillas_id);

        return redirect('home');
    }
}
