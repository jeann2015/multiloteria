<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdTickets extends Model
{
    protected $table="id_tickets";

    protected $fillable = [
        'id_tickets','id_tickets_temporals'
    ];

    public $timestamps = false;

    /**
     * @return int
     */
    public static function get_id_tickets_temporals(){
        $IdTickets = IdTickets::where('id_tickets_temporals','>',0)->get();

        if($IdTickets->count()>0) {
            IdTickets::where('id_tickets_temporals','>',0)
                ->update(['id_tickets_temporals'=>$IdTickets->pluck('id_tickets_temporals')[0]+1]);

            return $IdTickets->pluck('id_tickets_temporals')[0]+1;
        }else{
            IdTickets::create(['id_tickets_temporals'=>1,'id_tickets'=>0]);
            return 1;
        }
    }

    /**
     * @return int
     */
    public static function get_id_tickets(){
        $IdTickets = IdTickets::where('id_tickets','>',0)->get();

        if($IdTickets->count()>0) {

            IdTickets::where('id_tickets','>',0)
                ->update(['id_tickets'=>$IdTickets->pluck('id_tickets')[0]+1]);

            return $IdTickets->pluck('id_tickets')[0]+1;
        }else{

            IdTickets::create(['id_tickets'=>1,'id_tickets_temporals'=>0]);
            return 1;
        }
    }
}
