<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Gambling extends Model
{
    protected $table="gamblings";

    protected $fillable = [
        'users_id','description'
    ];

    /**
     * @return array
     */
    public function horas()
    {
        $hora = array(
            '12:00 AM'=>'12:00 AM',
            '12:30 AM'=>'12:30 AM',
            '01:00 AM'=>'01:00 AM',
            '01:30 AM'=>'01:30 AM',
            '02:00 AM'=>'02:00 AM',
            '02:30 AM'=>'02:30 AM',
            '03:00 AM'=>'03:00 AM',
            '03:30 AM'=>'03:30 AM',
            '04:00 AM'=>'04:00 AM',
            '05:00 AM'=>'05:00 AM',
            '05:30 AM'=>'05:30 AM',
            '06:00 AM'=>'06:00 AM',
            '06:30 AM'=>'06:30 AM',
            '07:00 AM'=>'07:00 AM',
            '07:30 AM'=>'07:30 AM',
            '08:00 AM'=>'08:00 AM',
            '08:30 AM'=>'08:30 AM',
            '09:00 AM'=>'09:00 AM',
            '09:30 AM'=>'09:30 AM',
            '10:00 AM'=>'10:00 AM',
            '10:30 AM'=>'10:30 AM',
            '11:00 AM'=>'11:00 AM',
            '11:30 AM'=>'11:30 AM',
            '12:00 PM'=>'12:00 PM',
            '12:30 PM'=>'12:30 PM',
            '01:00 PM'=>'01:00 PM',
            '01:30 PM'=>'01:30 PM',
            '02:00 PM'=>'02:00 PM',
            '02:30 PM'=>'02:30 PM',
            '03:00 PM'=>'03:00 PM',
            '03:30 PM'=>'03:30 PM',
            '04:00 PM'=>'04:00 PM',
            '04:30 PM'=>'04:30 PM',
            '05:00 PM'=>'05:00 PM',
            '05:30 PM'=>'05:30 PM',
            '06:00 PM'=>'06:00 PM',
            '06:30 PM'=>'06:30 PM',
            '07:00 PM'=>'07:00 PM',
            '07:30 PM'=>'07:30 PM',
            '08:00 PM'=>'08:00 PM',
            '08:30 PM'=>'08:30 PM',
            '09:00 PM'=>'09:00 PM',
            '09:30 PM'=>'09:30 PM',
            '10:00 PM'=>'10:00 PM',
            '10:30 PM'=>'10:30 PM',
            '11:00 PM'=>'11:00 PM',
            '11:30 PM'=>'11:30 PM',
        );

        return $hora;
    }

    /**
     * @return array
     */
    public function get_gamblings_time()
    {
        $taquillas_id = Session::get('taquillas_id');

        $LimitBooking = LimitBooking::where('id_taquilla', '=', $taquillas_id)
            ->select('limits_bookings.time_sub_gamblings_id')
            ->distinct('time_sub_gamblings_id')
            ->get();

        $g = new TimeSubGamblings;
        $hora_limite_actual = $g->get_hora_current();

        $gambling = Gambling::
        select('gamblings.description', 'gamblings.id')
            ->whereIN('id', $g->get_sub_gamblings_time($hora_limite_actual, $LimitBooking->toArray()))
            ->pluck('description', 'id');

        $gamblings = array_sort_recursive($gambling->toArray());

        return $gamblings;
    }

    /**
     * @param $sub_gamblings_id
     * @return array
     */
    public function get_sub_gamblings_time($sub_gamblings_id,$id_taquilla)
    {
        $g = new TimeSubGamblings;
        $hora_limite_actual = $g->get_hora_current();

        $LimitBooking = LimitBooking::where('id_taquilla', '=', $id_taquilla)
            ->select('limits_bookings.time_sub_gamblings_id')
            ->distinct('time_sub_gamblings_id')
            ->get();


        $gambling = SubGambling::select('sub_gamblings.description', 'sub_gamblings.id')
            ->whereIN('gamblings_id', $g->get_sub_gamblings_time($hora_limite_actual, $LimitBooking->toArray()))
            ->where('sub_gamblings.gamblings_id', $sub_gamblings_id)
            ->pluck('description', 'id');

        $gamblings = array_sort_recursive($gambling->toArray());

        return $gamblings;
    }

    /**
     * @param $sub_gamblings_id
     * @param $id_taquilla
     * @return array
     */
    public function get_time_sub_gamblings_time($sub_gamblings_id,$id_taquilla)
    {
        $g = new TimeSubGamblings;

        $LimitBooking = LimitBooking::where('id_taquilla', '=', $id_taquilla)
            ->select('limits_bookings.time_sub_gamblings_id')
            ->distinct('time_sub_gamblings_id')
            ->get();

        $hora_limite_actual = $g->get_hora_current();
        $time_gambling = $g->get_time_sub_gamblings_time($hora_limite_actual, $sub_gamblings_id,$LimitBooking->toArray());
        $gamblings = array_sort_recursive($time_gambling->toArray());

        return $gamblings;
    }



    public function get_time_sub_gamblings_time_temporal($sub_gamblings_id,$id_taquilla,$id_tickets_temporal)
    {
        $g = new TimeSubGamblings;



        $TicketsTemporal = TicketsTemporal::
            where('tickets_temporals.id_tickets_number',$id_tickets_temporal)
            ->select(
                'tickets_temporals.time_sub_gamblings_id')
            ->get();

        $LimitBooking = LimitBooking::where('id_taquilla', '=', $id_taquilla)
            ->whereNotIn('time_sub_gamblings_id',$TicketsTemporal->toArray())
            ->select('limits_bookings.time_sub_gamblings_id')
            ->distinct('time_sub_gamblings_id')
            ->get();

        $hora_limite_actual = $g->get_hora_current();
        $time_gambling = $g->get_time_sub_gamblings_time($hora_limite_actual, $sub_gamblings_id,$LimitBooking->toArray());
        $gamblings = array_sort_recursive($time_gambling->toArray());

        return $gamblings;
    }

    /**
     * @return array
     */
    public function get_gamblings_time_all()
    {
        $gambling = Gambling::
        select('gamblings.description', 'gamblings.id')
            ->pluck('description', 'id');

        $gamblings = array_sort_recursive($gambling->toArray());

        return $gamblings;
    }

    /**
     * @param $gamblings_id
     * @return mixed
     */
    public function get_time_sub_gamblings_time_all($gamblings_id)
    {
        $gambling = SubGambling::select('sub_gamblings.description', 'sub_gamblings.id')
            ->where('sub_gamblings.gamblings_id','=', $gamblings_id)
            ->pluck('description', 'id');

        return $gambling;
    }


}
