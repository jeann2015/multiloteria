<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $connection = 'mysql2';

    protected $table="taquillas";

    protected $fillable = [
          'id_taquilla' ,
          'nombre',
          'inactivo',
          'porcentaje',
          'porcentaje_derecho' ,
          'maximo_jugadas' ,
          'id_grupo',
          'alquilado' ,
          'proporcion',
          'mensaje',
          'por_derecho',
          'limite_tickets',
          'limite_diario',
          'limite_porderecho',
          'mostrar_reporte_seniat',
          'limite_parleys',
          'cantidad_tickets_borrar',
          'minutos_tickets_borrar',
          'porcentaje_fortuito',
          'limite_cobro',
          'minimo',
          'desactivar_ventas',
          'fecha_cierre_venta',
          'proporcion_limite_cobro',
          'porcentaje_parley_contribuyente',
          'porcentaje_derecho_contribuyente',
          'minimo_jugadas_ticket',
          'limite_ganancia_combinacion'
    ];

    public $timestamps = false;

}
