<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Sessiones extends Model
{
    /**
     * @return bool
     */
    public function search_session()
    {
        if (Session::has('users_id') && Session::has('taquillas_id')) {
            $taquillas_id = Session::get('taquillas_id');
            $b = Booking::where('id_taquilla', $taquillas_id)->get();

            $users_id = Session::get('users_id');
            $u = User::where('id_usuario', $users_id)->get();

            if ($b->count()>0 && $u->count()>0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
