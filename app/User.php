<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'mysql2';

    protected $table="usuarios";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'id_usuario',
          'nombre',
          'contraseña',
          'supervisor',
          'inactivo',
          'id_taquilla',
          'login',
          'id_grupo',
          'intermediario',
          'puerto_impresion',
          'generar_barcode',
          'paper_cut',
          'qzprint_active',
          'borrar_ticket',
          'version_venta',
          'email',
          'tesorero',
          'recargar_online',
          'zoom_modulos',
          'auto_parley',
          'agrupar_deportes_ventas',
          'comprimir_ticket',
          'id_rol',
          'access_only_access_token',
          'x_access_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
//    protected $hidden = [
//        'password', 'remember_token',
//    ];

    public $timestamps = false;
}
