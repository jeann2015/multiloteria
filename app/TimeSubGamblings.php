<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TimeSubGamblings extends Model
{
    protected $table="time_sub_gamblings";

    protected $fillable = [
        'gamblings_id','hora','sub_gamblings_id','status'
    ];

    /**
     * @return mixed
     */
    public function get_hora_current()
    {
        $hora_actuals = DB::select("select curtime() as hora");
        foreach ($hora_actuals as $hora_actual) {
            $hora_limite_actual=$hora_actual->hora;
        }
        return $hora_limite_actual;
    }

    /**
     * @return mixed
     */
    public function get_date_current()
    {
        $fecha_actuals = DB::select("select curdate() as date");
        foreach ($fecha_actuals as $fecha_actual) {
            $fecha_limite_actual=$fecha_actual->date;
        }
        return $fecha_limite_actual;
    }

    /**
     * @param $time
     * @return array
     */
    public static function get_sub_gamblings_time($time,$LimitBooking)
    {
        $time_sub_gamblings = self::where('hora','>=',$time)
            ->where('status',1)
            ->whereIN('time_sub_gamblings.id',$LimitBooking)
            ->select('time_sub_gamblings.gamblings_id')
            ->distinct()
            ->get()
            ->toArray();

        return $time_sub_gamblings;
    }

    /**
     * @param $time
     * @return array
     */
    public static function get_time_sub_gamblings_time($time,$sub_gamblings_id,$LimitBooking)
    {
        $time_sub_gamblings = self::where('hora','>=',$time)
            ->select(
                'time_sub_gamblings.id',
                \DB::raw("TIME_FORMAT(time_sub_gamblings.hora, '%h:%i %p') as hora"
                )
            )
            ->where('sub_gamblings_id',$sub_gamblings_id)
            ->whereIN('id',$LimitBooking)
            ->pluck('time_sub_gamblings.hora','time_sub_gamblings.id');

        return $time_sub_gamblings;
    }

    /**
     * @param $sub_gamblings_id
     * @return mixed
     */
    public static function get_time_sub_gamblings_time_all($sub_gamblings_id)
    {
        $time_sub_gamblings = self::
            select(
                'time_sub_gamblings.id',
                \DB::raw("TIME_FORMAT(time_sub_gamblings.hora, '%h:%i %p') as hora"
                )
            )
            ->where('sub_gamblings_id',$sub_gamblings_id)
            ->where('status',1)
            ->pluck('time_sub_gamblings.hora','time_sub_gamblings.id');

        return $time_sub_gamblings;
    }

}
