<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $table="rewards";

    protected $fillable = [
        'time_sub_gamblings_id','users_id'
    ];
}
