<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketsWinning extends Model
{
    protected $table="tickets_winnings";

    protected $fillable = [
        'users_id','sub_gamblings_id','amount','id_taquilla','hora_sub_gamblings','gamblings_id',
        'time_sub_gamblings_id','id_tickets_number','serial'
    ];
}
