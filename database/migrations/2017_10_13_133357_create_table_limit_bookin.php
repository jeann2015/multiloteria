<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLimitBookin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('limits_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_gamblings_id')->unsigned();
            $table->integer('time_sub_gamblings_id')->unsigned();
            $table->integer('id_taquilla')->unsigned();
            $table->integer('awards_id')->unsigned();
            $table->decimal('amount',12,2);
            $table->timestamps();

            $table->foreign('sub_gamblings_id')->references('id')->on('sub_gamblings');
            $table->foreign('time_sub_gamblings_id')->references('id')->on('time_sub_gamblings');
            $table->foreign('awards_id')->references('id')->on('awards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('limits_bookings');
    }
}
