<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeSubGamblingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_sub_gamblings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gamblings_id')->unsigned();
            $table->integer('sub_gamblings_id')->unsigned();
            $table->smallInteger('status');
            $table->time('hora');
            $table->timestamps();

            $table->foreign('gamblings_id')->references('id')->on('gamblings');
            $table->foreign('sub_gamblings_id')->references('id')->on('sub_gamblings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_sub_gamblings');
    }
}
