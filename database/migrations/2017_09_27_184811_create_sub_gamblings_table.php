<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubGamblingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_gamblings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->integer('gamblings_id')->unsigned();
            $table->smallInteger('status');
            $table->timestamps();

            $table->foreign('gamblings_id')->references('id')->on('gamblings');

        });

        \DB::statement("ALTER TABLE sub_gamblings ADD COLUMN `images`  LONGBLOB AFTER `status`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_gamblings');
    }
}
