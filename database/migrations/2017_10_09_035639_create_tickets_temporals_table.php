<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTemporalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets_temporals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_taquilla')->unsigned();
            $table->integer('gamblings_id')->unsigned();
            $table->integer('time_sub_gamblings_id')->unsigned();
            $table->integer('sub_gamblings_id')->unsigned();
            $table->time('hora_sub_gamblings');
            $table->integer('users_id')->unsigned();
            $table->decimal('amount',12,2);
            $table->integer('id_tickets_number')->unsigned();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets_temporals');
    }
}
